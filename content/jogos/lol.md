---
title: "League of Legends"
date: 2021-09-18T23:27:57-03:00
draft: false
---

{{< figure src=https://images.squarespace-cdn.com/content/v1/5c0fd99e4eddecc30ab724b9/1566837158734-LAW2B2Y72QDT268F4DUA/LoL-square-button.jpg?format=1000w  height=250px width=200px class="img_content">}}

Com certeza o jogo que mais joguei na vida. League of Legends, ou lol para os íntimos, é um jogo Moba de estratégia em que os membros da equipe precisam unir forças para derrotar a equipe adversária. Pode ser um pouco chato no início até entender o funcionamento, mas a parte competitiva logo acaba cativando a maioria dos players.