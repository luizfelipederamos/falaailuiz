---
title: "Skyrim"
date: 2021-09-18T23:27:57-03:00
draft: false
---


{{< figure src=https://images.kabum.com.br/produtos/fotos/140600/the-elder-scrolls-v-skyrim_1611085424693_g.jpg height=250px width=200px class="img_content">}}

Skyrim é um jogo que precisa ser jogado por todos aqueles que gostam minimamente de RPG. Com uma história envolvente e uma jogabilidade impecável, Skyrim com certeza é um dos melhores jogos já feitos na história.