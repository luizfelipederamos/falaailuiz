---
title: "Ilha do Medo"
date: 2021-09-18T23:27:57-03:00
draft: false
---
{{< figure src=https://1.bp.blogspot.com/-WK8tgMxcwsw/WD81iYYm3dI/AAAAAAAADqg/6z-m5QRewP4zgWuRt5R655inbPvq0-2xACLcB/s1600/ILHA-DO-MEDO.jpg height=250px width=170px class="img_content">}}

Com certeza o melhor filme que já assisti, atuação impecável do DiCaprio e do Mark Ruffalo (o Hulk de Vingadores). O filme te prende do início ao fim e é muito importante prestar atenção em tudo para não perder nenhum detalhe. Super indico para quem gosta de filmes de suspense com alguns elementos de investigação e terror psicológico.
