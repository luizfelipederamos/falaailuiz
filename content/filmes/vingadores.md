---
title: "Vingadores Ultimato"
date: 2021-09-18T23:27:57-03:00
draft: false
---
{{< figure src=https://br.web.img3.acsta.net/pictures/19/04/26/17/30/2428965.jpg height=250px width=170px class="img_content" >}}

Excelente filme, bom desfecho para a história. Conta com um bom balanceamento entre cenas de ação e explicações sobre a situação dos personagens. Com certeza arrancou grandes emoções dos entusiastas da franquia Marvel.