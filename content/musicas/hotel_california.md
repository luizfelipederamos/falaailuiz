---
title: "Hotel California"
date: 2021-09-18T23:27:57-03:00
draft: false
---

{{< figure src=https://conteudo.imguol.com.br/c/entretenimento/f6/2018/01/18/a-banda-eagles-1516311027182_v2_450x337.jpg  height=250px width=230px class="img_content">}}

Uma das primeiras músicas que o meu pai me mostrou, por isso possui um apelo sentimental muito grande. Mesmo assim, é uma excelente música e dificilmente não irá agradar os apreciadores de soft rock.