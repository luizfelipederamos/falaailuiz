---
title: "Burguesinha"
date: 2021-09-18T23:27:57-03:00
draft: false
---

{{< figure src=https://studiosol-a.akamaihd.net/uploadfile/letras/albuns/1/4/4/c/219731442343369.jpg height=250px width=230px class="img_content">}}

O albúm inteiro é uma obra-prima. O título "músicas para churrasco" exprime bem a pegada leve e descontraída que a seleção apresenta. Mesmo assim, destaco aqui a música Burguesinha, que é a melhor do Seu Jorge e com certeza merece todos os elogios recebidos

